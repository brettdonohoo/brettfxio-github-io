---
# the default layout is 'page'
icon: fas fa-info-circle
order: 4
---

🧑‍💻 🚀🎵🍎 Part-time Dad, 37/M/UT/5'5/Single, Thoughts here are my own, not employer's (DoD). Not a financial advisor!

- [Our Foundation](https://digitaleducationsafety.org)
- [My Company](https://functions.io)